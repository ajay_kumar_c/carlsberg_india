#!/usr/bin/python
import sys
import os

secrets = ['CPG_DB_HOST','CPG_DB_NAME','CPG_DB_USERNAME','CPG_DB_PASSWORD',]

repo_home = None

# repo_home
repo_home = os.environ.get('REPO_HOME','')

#set kettle repositoriy
repo_home_xml = repo_home.replace('/','&#x2f;')
with open('.kettle/repositories.xml', 'r') as myfile:
    data=myfile.read().replace('{{REPO_ROOT}}', repo_home_xml)

with open('.kettle/repositories.xml', 'w') as myfile:
    myfile.write(data)

#set etlconfig.properties
data=''
with open('config/etlconfig.properties', 'r') as myfile:
    data=myfile.read()

    if repo_home:
    	data = data.replace('{{REPO_ROOT}}', repo_home)

    for secret in secrets:
    	if os.environ.get(secret):
    		print 'secret {0} found'.format(secret)
	    	data = data.replace('{{' + secret + '}}', os.environ.get(secret,''))

# write dwconfg
with open('config/etlconfig.properties', 'w') as myfile:
    myfile.write(data)
