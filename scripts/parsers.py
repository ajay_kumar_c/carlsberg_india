#!/usr/bin/python
import csv
import openpyxl
import sys
import traceback
import re

# output fields
output_fields = ['']

# class BaseParser():

def winspirit_parse_row(sheet, row_no,base_vals,header_row_no):
	vals = dict(base_vals)
	vals['retailer_name'] = sheet.cell(row=row_no,column=2).value
	# print 'processsing row {0} with retailer_name {1}'.format(row_no,vals['retailer_name'])
	salesman = False

	records = []
	header_row = sheet.rows[header_row_no]
	field_names = [x.value for x in header_row]
	# print field_names
	for idx, field_name in enumerate(field_names):
		column_no = idx + 1
		if field_name:
			field_name = field_name.strip()	
		else:
			continue

		if field_name == 'PARTY_NAME' or field_name == 'TOTAL':
			continue

		if field_name == 'SALESMAN':
			salesman = sheet.cell(row=row_no,column=column_no).value
			continue

		#each column is a product
		vals['product_name'] = field_name
		quantity = str(sheet.cell(row=row_no,column=column_no).value)
		if quantity:
			try:
				quantity = quantity.replace('(0)','').strip()
				# quantity = re.sub(r'\s+', '', quantity)
				quantity = int(float(quantity))
			except ValueError as err:
				message =  "while converting {0} - got error {1}".format(quantity, traceback.format_exception_only(type(err), err))
				quantity =  0
			except Exception as err2:
				print "while converting {0} - got error {1}".format(quantity, traceback.format_exception_only(type(err2), err2))
				quantity = -6

		else: 
			quantity = 0
		vals['quantity'] = quantity
		records.append(dict(vals))

	#add salesman to the records
	for record in records:
		record['seller_name'] = salesman

	return records


def winspirit_parse_file(input_file_path, base_vals):	
	records = []	
	header_row = False

	#parse the excel file
	wb = openpyxl.load_workbook(input_file_path)
	sheet = wb.worksheets[0]

	data_row = False
	#first identify the data start and data end rows. 
	data_start_row = False
	data_end_row = False
	header_row_no = False
	for row_no in range(1,sheet.max_row):

		#identify the data row
		if sheet.cell(row=row_no,column=2).value and isinstance(sheet.cell(row=row_no,column=2).value,basestring) and (sheet.cell(row=row_no,column=2).value.strip().lower() in ['party_name', 'party name']):		
			# data_row = True #from next, it would be data
			data_start_row = row_no + 1
			# header_row = sheet.rows[row_no - 1]
			header_row_no = row_no - 1
			data_end_row = sheet.max_row
			# print 'header row {0} with vals {1}'.format(row_no,header_row)
			break

	if not data_start_row:
		print "Data start row could not be found"
		return []

	for row_no in range(data_start_row, data_end_row):
		row_records = winspirit_parse_row(sheet, row_no,base_vals,header_row_no)
		if row_records:
			records.extend(row_records)

	return records

def tallyerp9_parse_row(sheet, row_no,base_vals,header_row):
	vals = dict(base_vals)
	vals['salesman'] = False
	vals['retailer_name'] = sheet.cell(row=row_no,column=1).value
	salesman = False

	records = []
	field_names = [x.value for x in header_row]
	print 'header rows: ', field_names

	for idx, field_name in enumerate(field_names):

		column_no = idx + 1
		if field_name:
			field_name = field_name.strip()	
		if not field_name:
			continue

		#each column is a product
		vals['product_name'] = field_name
		quantity = sheet.cell(row=row_no,column=column_no).value
		if quantity:
			vals['quantity'] = quantity
			records.append(dict(vals))
	return records


def tallyerp9_parse_file(input_file_path, base_vals):	
	records = []	
	header_row = False

	#parse the excel file
	wb = openpyxl.load_workbook(input_file_path)
	sheet = wb.worksheets[0]

	#first identify the data start and data end rows. 
	data_start_row = False
	data_end_row = False
	header_row_no = False
	for row_no in range(1,sheet.max_row+1):
		if sheet.cell(row=row_no, column=1).value == 'Movement Outward :' and sheet.cell(row=(row_no+1), column=1).value == 'Buyers :':
			data_start_row = row_no + 2
			header_row_no = data_start_row - 8
			header_row = sheet.rows[header_row_no]
			# print 'header row: ', 

		if data_start_row and not sheet.cell(row=row_no,column=1).value:
			data_end_row = row_no

	print "data_start_row: {0}, data_end_row: {1}, header_row: {2}".format(data_start_row, data_end_row, header_row)
	#process each of the data rows

 	for row_no in range(data_start_row, data_end_row):
		row_records = tallyerp9_parse_row(sheet, row_no,base_vals,header_row)
		if row_records:
			records.extend(row_records)

	return records