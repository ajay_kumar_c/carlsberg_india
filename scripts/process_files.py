#!/usr/bin/python
import csv
import openpyxl
import sys,traceback
# import boto3
from boto3.session import Session

import parsers

# output fields
output_fields = ['distributor_id','distributor_name','invoice_date','upload_date','retailer_name','product_name','quantity','seller_name']
billing_softwares = ['Win_Spirit','TallyERP9','Pharm Soft']

#download the file from s3
def download_s3_file(aws_access_key, aws_secret_key,aws_bucket_region,aws_bucket_name,aws_file_path,local_file_path):
	aws_file_path = aws_file_path.replace('https://ivydevbkt.s3.amazonaws.com/','')
	try:
		session = Session(aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key, region_name=aws_bucket_region)
		# print "trying to download with access_key:{0} \t secret_key:{1} \t bucket_region:{2} \t bucket_name: {3} \t file_path: {4}".format(aws_access_key,aws_secret_key,aws_bucket_region,aws_bucket_name,aws_file_path)
		s3_client = session.client('s3')
		s3_client.download_file(aws_bucket_name, aws_file_path,local_file_path)
	except Exception as err:
		# sys.stderr.write()
		sys.exit("Error while downloading file from S3." + str(traceback.format_exception_only(type(err), err)) + ". AWS bucket: {0} AWS File: {1}".format(aws_bucket_name,aws_file_path))
	return True

def parse_file(local_file_path,base_vals,billing_software):
	#first select the right parser
	if billing_software == 'Win_Spirit':
		return parsers.winspirit_parse_file(local_file_path,base_vals)
	if billing_software == "TallyERP9" or billing_software == "Pharm Soft":
		return parsers.tallyerp9_parse_file(local_file_path,base_vals)

	sys.exit("No parser found for Billing software {0}".format(billing_software))

	return

def write_to_csv(records,output_path):
	print "in write to csv for " + output_path
	if not records:
		records = []
	with open(output_path, 'w') as csvfile:
		# writer = csv.writer(csvfile, delimiter='|',quoting=csv.QUOTE_MINIMAL)
		writer = csv.writer(csvfile, delimiter='|')
		print "wrote to field list to {0}".format(output_path)
		writer.writerow(output_fields)
		for record in records:
			output_row = []
			for field_name in output_fields:
				output_row.append(record.get(field_name,False))
			writer.writerow(output_row)

#begin of script
if __name__ == "__main__":
	#parameters received
	# print "received ",sys.argv
	arg_sep = ':::'

	arg_full = sys.argv[1]
	arg_full = arg_full[arg_full.find(' ') + 1:-1]    #remove the python script path from the single line arg
	args = arg_full.split(arg_sep)   #split the args
	# print 'parsed args: ', args
	aws_access_key = args[0] 
	aws_secret_key = args[1]
	aws_bucket_region = 'ap-southeast-1'
	aws_bucket_name = args[3]
	aws_file_path = args[4]
	distributor_id = args[5]
	distributor_name = args[6]
	invoice_date = args[7]
	billing_software = args[8]
	upload_id = args[9]
	upload_date = args[10]
	print "upload_id is {0}".format(upload_id)

	if billing_software:
		billing_software = billing_software.strip()

	if not billing_software in billing_softwares:
		sys.exit("No parser found for Billing software {0}".format(billing_software))


	# if not aws_access_key:
	# 	aws_access_key = 'AKIAI5OG2UQYXDPYQNNQ'
	# if not aws_secret_key:
	# 	aws_secret_key = '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'
	# if not aws_bucket_region:
	# 	aws_bucket_region = 'ap-southeast-1'
	# if not aws_bucket_name:
	# 	aws_bucket_name = 'ivydevbkt'

	# distributor_id = False
	# distributor_name = False
	# invoice_date = False
	# upload_date = False
	# if not distributor_id:
	# 	distributor_id = 4
	# if not distributor_name:
	# 	distributor_name = 'SWASTIK ENTERPRISES'
	# if not invoice_date:
	# 	invoice_date = '2016-02-15'
	# if not upload_date:
	# 	upload_date = '2016-02-15'

	#download the file
	local_file_name = '/tmp/register_' + upload_id + '.xlsx'
	result = download_s3_file(aws_access_key,aws_secret_key,aws_bucket_region,aws_bucket_name,aws_file_path,local_file_name)


	base_vals = {'distributor_id': distributor_id, 'distributor_name':distributor_name,'invoice_date':invoice_date,'upload_date':upload_date}
	records = parse_file(local_file_name,base_vals, billing_software)
	# print "received records: ", records
	write_to_csv(records,'/tmp/output_' + upload_id + '.csv')
	sys.exit(0)
	# print records
	# if result:
		# parse the file
		# base_vals = {'distributor_id': distributor_id, 'distributor_name':distributor_name,'invoice_date':invoice_date,'upload_date':upload_date}
		# records = parse_file(local_file_name,base_vals)
		# print 'here are the records: ', records



